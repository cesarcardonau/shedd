###############
##Microbial Ecology Lab
##PI:Jack Gilbert
##Code:Cesar Cardona
##Last Update: July 1 2016
###############
#rm(list=ls())
library(ggplot2)
require(gplots)
require(Deducer)
library(plyr)
###############
##Read mapping file -only samplesid,surfacetype (Source2), Date, Time
##Helper is sorted by date and time and removed those entries with not info in those two vars
###############
filehelper="inputfiles/shedd.IL.map.core.dt2.helper" 
map_file="inputfiles/shedd.IL.map.core.square"
helper<- read.table(paste(filehelper), header=FALSE, row.names = 1,sep="",stringsAsFactors = FALSE)
colnames(helper)=c("Source2","Date","Time")
helper$prepost=as.factor(ifelse(helper$Date<=18,"_Pre","Post"))
helper$prepost3=ifelse(helper$Date %in% c(16,17,18),"_Pre-3day",ifelse(helper$Date %in% c(20,21,22),"Post-3day",NA))
map<- read.table(paste(map_file), header=TRUE, row.names = 1,sep="\t",fill=TRUE,stringsAsFactors = FALSE)
map$Group = ifelse(map$Source=="dolphin",ifelse(map$Individual %in% c('D1','D2'),'Group_2','Group_1'),NA)
map1<-map[,c("Individual","Group1","Group2","OnlyBefore","OnlyAfter","Group")]
helper1<-merge(helper,map1,by="row.names")
rownames(helper1)=helper1[,1]
helper1=helper1[,-1]#remove rownames
###############
##Function to create plots
###############
create_plots_univariate <-function(beta_ds0,helper0=helper1,surface0="Unknown") {
  ##DEBUG 
  #beta_ds0=wunifrac
  #surface0=surface
  #helper0=helper1
  beta_name=deparse(substitute(beta_ds0))
  merged <-merge(helper0, beta_ds0,by="row.names")
  rownames(merged)=merged[,1]
  merged1 <-  merged[with(merged,order(Individual,Date,Time,Row.names)),]
  ##sequence unifrac only makes sense for the same individual
  split1 <- split(merged1,merged1$Individual)
  seq_field <- ldply(split1,function(ls_individual) {
    ds_individual=as.data.frame(ls_individual)
    print(dim(ds_individual))
    #making a squared matrix again
    sorted=merged1[c(rownames(ds_individual)),c(rownames(ds_individual))]
    #check
    print(table(colnames(sorted)==rownames(sorted)))
    ##find distance in the matrix for consecutive times, x and x+1, and cycle again
    listofsequnifracs=sapply(1:(nrow(sorted)-1),function(x) {return(sorted[x,x+1])})
    return(as.data.frame(listofsequnifracs))
  })
  #plot field
  strtitle=paste("Pairwise ",beta_name," distances comparision ",surface0, sep="")
  ggplot(seq_field,aes(x=listofsequnifracs,y = ..density.., fill=surface0))+geom_density(alpha=0.3)+xlim(0,1.29)+ggtitle(strtitle)+
    scale_fill_manual("Surface",values=c("yellow"))
  ggsave(paste(beta_name,"-",surface0,".png",sep=""),width=7.5,height=5)
}


create_plots_bivariate <-function(beta_ds0,helper0=helper1,surface0="Unknown",fieldsplit0="prepost",file0=stdout()) {
##DEBUG 
#beta_ds0=uwunifrac
#surface0=surface
#fieldsplit0="prepost"
#helper0=helper1
  merged <-merge(helper0, beta_ds0,by="row.names")
  rownames(merged)=merged[,1]
  merged1 <-  merged[with(merged,order(Individual,Date,Time,Row.names)),]
  #sorted <-  merged1[,c(rownames(merged1))]
  ##sequence unifrac only makes sense for the same individual
  split1 <- split(merged1,merged1$Individual)
  seq_field <- ldply(split1,function(ls_individual) {
    ds_individual=as.data.frame(ls_individual)
    print(dim(ds_individual))
    #making a squared matrix again
    sorted=merged1[c(rownames(ds_individual)),c(rownames(ds_individual))]
    #check
    table(colnames(sorted)==rownames(sorted))
    ##find distance in the matrix for consecutive times, x and x+1, and cycle again
    listofsequnifracs=sapply(1:(nrow(sorted)-1),function(x) {return(sorted[x,x+1])})
    ##Add new field to dataframe - first distance NA to preserve number of distances equal to original rows
    ds_individual$sequnifrac=c(NA,listofsequnifracs)
    seq=ds_individual[,c("Row.names","Date","Time","sequnifrac","prepost",fieldsplit0)]
    seq_all=seq[-1,]
    return(seq_all[complete.cases(seq_all[,c(fieldsplit0)]),c(fieldsplit0,"sequnifrac")])
  })
  #p values 
  value1=unique(seq_field[,c(fieldsplit0)])[1]
  value2=unique(seq_field[,c(fieldsplit0)])[2]
  dist1_field=seq_field[seq_field[,fieldsplit0]==value1,c("sequnifrac")]
  dist2_field=seq_field[seq_field[,fieldsplit0]==value2,c("sequnifrac")]
  set.seed(14)
  p_value_field=perm.t.test(dist1_field,dist2_field,statistic="t",alternative="two.sided",B=9999)[[2]]
  write.table(paste(surface0,"|",deparse(substitute(beta_ds0)),"|",fieldsplit0,"|",p_value_field),
              file = file0,append = TRUE,
              col.names=FALSE,row.names=FALSE,sep="|",quote = FALSE)
  #plot field
  strtitle=paste("Pairwise ",deparse(substitute(beta_ds0))," distances comparision ",surface0," (p=",p_value_field,")",sep="")
  ggplot(seq_field,aes(x=sequnifrac,y = ..density.., fill=get(fieldsplit0)))+geom_density(alpha=0.3)+xlim(0,1.29)+ggtitle(strtitle)+
    scale_fill_manual("Probiotics",values=c("yellow","red"))
  ggsave(paste(deparse(substitute(beta_ds0)),"-",surface0,"-",fieldsplit0,".png",sep=""),width=8,height=5,units = "in")
}
###############
##Part 1 - Create plots and stats for distinct surfaces at Source2 level - edited so they follow the same individual
###############
surfaceall=c("human-nose"
             ,"human-hand"
             ,"food"
             ,"water"
             ,"air"
             ,"dolphin-rectum"
             ,"dolphin-skin"
             ,"dolphin-chuff"
)
surface_dolph=surfaceall[6:8]
###General compare PrePos
sapply(surfaceall,function(surface){
##DEBUG
##surface="dolphin-nose"
 # surfacetext=deparse(substitute(surface))
  
  print(surface)
  wfile_pre="weighted_unifrac_shedd.IL.core.dt2__Source2_"
  uwfile_pre="unweighted_unifrac_shedd.IL.core.dt2__Source2_"
  #bcfile_pre="bray_curtis_shedd.IL.core.dt2__Source2_"
  file_pos="__.txt.gz"
  wunifrac<- read.table(paste("inputfiles/pairwise_beta_div/",wfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  uwunifrac<- read.table(paste("inputfiles/pairwise_beta_div/",uwfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  ##choset to create plots for one beta diverstivy at the time
  #create_plots_univariate(wunifrac,surface0= surface)
  #create_plots_univariate(uwunifrac,surface0= surface)
  create_plots_bivariate(uwunifrac,surface0= surface,fieldsplit0 = "prepost",file0 = paste0("uwu_pairwise.stats.txt",sep=""))
  create_plots_bivariate(wunifrac,surface0= surface,fieldsplit0 = "prepost",file0 = paste0("wu_pairwise.stats.txt",sep=""))
})
###Compare individual dolphin Groups
sapply(surface_dolph,function(surface){
  ##DEBUG
  ##surface="human-nose"
  print(surface)
  wfile_pre="weighted_unifrac_shedd.IL.core.dt2__Source2_"
  uwfile_pre="unweighted_unifrac_shedd.IL.core.dt2__Source2_"
  #bcfile_pre="bray_curtis_shedd.IL.dolph.dt2__Source2_"
  
  file_pos="__.txt.gz"
  wunifrac<- read.table(paste("inputfiles/pairwise_beta_div/",wfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  uwunifrac<- read.table(paste("inputfiles/pairwise_beta_div/",uwfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  #braycurtis<- read.table(paste("inputfiles/pairwise_beta_div/",bcfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")

  #create_plots_bivariate(uwunifrac,surface0 = surface,fieldsplit0 = "Group",file0 = "pairwise_stats2.txt")
  #create_plots_bivariate(wunifrac,surface0= surface,fieldsplit0 = "Group",file0 = "pairwise_stats2.txt")
  
  create_plots_bivariate(uwunifrac,surface0 = surface,fieldsplit0 = "Group1",file0 = "uwu_pairwise_stats_group.txt")
  create_plots_bivariate(uwunifrac,surface0= surface,fieldsplit0 = "Group2",file0 = "uwu_pairwise_stats_group.txt")
  # create_plots_bivariate(wunifrac,surface0 = surface,fieldsplit0 = "Group1",file0 = "wu_pairwise_stats_group.txt")
  # create_plots_bivariate(wunifrac,surface0= surface,fieldsplit0 = "Group2",file0 = "wu_pairwise_stats_group.txt")
  # 
  
})
###############
##Part 2 - Create plots for distinct particular dolphin sources at Source3 - they follow the same individual
###############
surface3all=c(
  "dolphin-stool-KATRL"
  ,"dolphin-skin-KATRL"
  ,"dolphin-nose-KATRL"
  ,"dolphin-stool-KRI"
  ,"dolphin-skin-KRI"
  ,"dolphin-nose-KRI"
  ,"dolphin-stool-TIQUE"
  ,"dolphin-skin-TIQUE"
  ,"dolphin-nose-TIQUE"
  ,"dolphin-stool-SAGU"
  ,"dolphin-skin-SAGU"
  ,"dolphin-nose-SAGU"
)
sapply(surface3all,function(surface){
  ##DEBUG
  ##surface="dolphin-stool-KATRL"
  print(surface)
  wfile_pre="weighted_unifrac_shedd.IL.dolph.dt2__Source3_"
  uwfile_pre="unweighted_unifrac_shedd.IL.dolph.dt2__Source3_"
  file_pos="__.sorted.txt"
  wunifrac<- read.table(paste("inputfiles/pairwise_betadiv/",wfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  uwunifrac<- read.table(paste("inputfiles/pairwise_betadiv/",uwfile_pre,surface,file_pos,sep=""), header=TRUE, row.names = 1,sep="\t")
  create_plots_bivariate(uwunifrac,surface0= surface,fieldsplit0 = "prepost")
  create_plots_bivariate(uwunifrac,surface0= surface,fieldsplit0 = "prepost3")
  
})
#############heat map p-vales results from above#################

#Maple Heatmap based on Z-Score of OTU abundance
Data1 <- read.table("inputfiles/pairwise_stats.txt",header=FALSE,sep="|")
Data1a <- reshape(Data1[,-2], 
             timevar = "V3",
             idvar = c("V1"),
             direction = "wide")
Data1a <- arrange(Data1a,V1)
rownames(Data1a) <- paste(Data1a$V1)
Data1b<- as.matrix(round(Data1a[,2:ncol(Data1a)],2))

Data2 <- read.table("inputfiles/pairwise_stats2.txt",header=FALSE,sep="|")
Data2a <- reshape(Data2[,-2], 
                  timevar = "V3",
                  idvar = c("V1"),
                  direction = "wide")
Data2a <- arrange(Data2a,V1)
rownames(Data2a) <- paste(Data2a$V1)
Data2b<- as.matrix(round(Data2a[,2:ncol(Data2a)],2))
#h=heatmap.2(Data1b,scale="row",Colv=TRUE,dendrogram="row",col=palette,trace="none",
#            density.info="none",cexRow=0.8,cexCol=0.8,margins=c(4,20),srtCol=10)
#Make Heatmap of log-transformed OTU abundance 
#Log_Data1b <- log(Data1b + 1) 
#Make Color Palette for Heatmap
#http://www.stat.columbia.edu/~tzheng/files/Rcolor.pdf
palette <- colorRampPalette(c("white","deepskyblue","deepskyblue4","blue4"))(n = 4)

png(file = "paiwise-heat-summary.png",width=800,height=600)
h1=heatmap.2(Data1b,scale="none",Colv=FALSE,Rowv=FALSE,dendrogram="none",col=palette,na.color="black",
          density.info="none",cexRow=0.8,cexCol=0.8,margins=c(4,10),trace="none",srtCol=10,
          symkey=F,breaks = c(0,0.01,0.05,0.1,1),
          sepwidth=c(0.05,0.05),sepcolor="grey80",colsep=1:ncol(Data1b),rowsep=1:nrow(Data1b))
dev.off()

png(file = "paiwise-heat-summary-2.png",width=800,height=600)
h2=heatmap.2(Data2b,scale="none",Colv=FALSE,Rowv=FALSE,dendrogram="none",col=palette,na.color="black",
          density.info="none",cexRow=0.8,cexCol=0.8,margins=c(4,10),trace="none",srtCol=10,
          symkey=F,breaks = c(0,0.01,0.05,0.1,1),
          sepwidth=c(0.05,0.05),sepcolor="grey80",colsep=1:ncol(Data2b),rowsep=1:nrow(Data2b))
dev.off()
