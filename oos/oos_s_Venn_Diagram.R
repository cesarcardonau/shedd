

rm(list=ls())
t1 <- read.table("inputfiles/shedd.Venn.nose.skin.stool.air.txt", header=FALSE, sep="\t")
t_cnt <- read.table("inputfiles/shedd.IL.read_cnt_per_otu.txt", header=FALSE, sep="|")


Nose = t1$V1
Skin = t1$V2
Stool = t1$V3
Air = t1$V4
otu = t_cnt$V2
otu_rank=rownames(t_cnt)
otu_taxa=t_cnt$V4
otu_cnt=t_cnt$V1

inter1=intersect(intersect(intersect(Nose,Air),Skin),Stool)
length(inter1)
##3350
inter_check=intersect(inter1,otu)
length(inter_check)
##3350 OKAY
##filter dataset of counts, way 1
ind_intersect=match(inter1,otu)
otus_intersect=data.frame(otu[ind_intersect],otu_cnt[ind_intersect],otu_taxa[ind_intersect])
##filter dataset of counts, way 2
otus_intersect2=subset(t_cnt, otu %in% inter1)
write.table(otus_intersect2, file='otus_intersection_Venn.txt')

library(VennDiagram)
# Figure 2-9
venn.plot <- venn.diagram(
  x = list(    Nose=Nose,    Skin=Skin,    Stool=Stool,    Air=Air),
  filename = "shedd.tiff",
  cex = 1.5,
  cat.cex = 1.5,
  cat.pos = 1,
  cat.dist = c(0.04, 0.04, 0.04,0.04),
  fill = c("orange", "red", "green", "blue"),
  reverse = TRUE
)
