#!/usr/bin/env Rscript
###############
##Microbial Ecology Lab
##PI:Jack Gilbert
##Code:Cesar Cardona
##10-06-16 Initial Codo - outputfile should be sorted by surface-time in order to do next steps
###############
rm(list=ls())
###############
filenamein="inputfiles/shedd.IL.norare.core.dt1.col.top.txt.nohash.gz"
metadatain="inputfiles/shedd.IL.norare.core.dt1.col.num_samples_per_col.gz"

args = commandArgs(trailingOnly=TRUE)
if (length(args)!=0) {
  filenamein=args[1]
  metadatain=args[2]
}  else {
  warning("At least two argument musts be supplied (input file).\nContinuing with defaults", call.=FALSE)
}
otu<- read.table(filenamein, header=TRUE, sep="\t",stringsAsFactors = FALSE,check.names=FALSE)
metadata<- read.table(metadatain, header=FALSE, sep="\t",stringsAsFactors = FALSE)

rownames(otu)=otu$OTUID
asNumeric <- function(x) as.numeric(as.character(x))
factorsNumeric <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                   asNumeric))
factorsCharacter <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                   as.character))
#otu=factorsCharacter(otu)
colnames(metadata)=c('Group','PriorSites','Norm')
metadata$Location=as.factor(gsub("\\..*$", "", metadata$Group))
metadata$Day=as.factor(gsub("^.*\\.", "", metadata$Group))
metadata$PrePost=as.factor(ifelse(as.numeric(as.character(metadata$Day))<=18,'Pre','Post'))
metadata$Condition=as.factor(paste0(metadata$Location,".",metadata$PrePost))
metadata=metadata[order(metadata$Location,asNumeric(metadata$Day)),]
#metadata$Test1=ifelse(metadata$Condition=="dolphin.nose.Post"|metadata$Condition=="human.hand.Post","ComboPost",
#                  ifelse(metadata$Condition=="dolphin.nose.Pre"|metadata$Condition=="human.hand.Pre","ComboPre","NotCombo"))
rownames(metadata)=metadata$Group

##Unsured if I need to pass this version of normalized counts to bnanjo, most likely yes
#lapply(metadata$Group,function(group_i) {otu[,gsub("-",".",group_i)]})
#lapply(metadata$Group,function(group_i) {as.integer(metadata[metadata$Group==group_i,'Norm'])})
##otu1 columns should line up with otu norm column for the lapply to work
##lapply allows columns to be in different order than mapping metadata column
otu1=otu[,-c(1,ncol(otu))]
rownames(otu1)=otu$OTUID
otu_norm=otu1
otu_norm[]=as.data.frame(lapply(colnames(otu1),function(group_i){
  return(round(otu1[,group_i]/as.integer(metadata[metadata$Group==group_i,'Norm'])))
}))
otu_norm1=(otu_norm+1)
otu_norm1=otu_norm1[,c(rownames(metadata))]
otu_norm1=modifyList(otu_norm1,lapply(otu_norm1,as.integer))
####check
print("Checking counts data and metadata ...")
table(colnames(otu_norm1)==rownames(metadata))

library(DESeq2)
#construct DESeq object from input matrix (countdata)
dds_def = DESeqDataSetFromMatrix(countData = otu_norm1, colData = metadata, 
                          #   design = ~ group + treatment + group:treatment)
                          design = ~ Condition)
dds1 = DESeq(dds_def)
###
#Estimating size factors
#Error in estimateSizeFactorsForMatrix(counts(object), locfunc = locfunc,  : 
# every gene contains at least one zero, cannot compute log geometric means, adding +1 to counts to solve
#how to understand results https://rdrr.io/bioc/DESeq2/man/results.html
dds1@design
dds1@colData
resultsNames(dds1)
result_A_B=list()
result_A_B[1]=results(dds1,contrast=c("Condition","dolphin-chuff.Post","dolphin-chuff.Pre"))
result_A_B[2]=results(dds1,contrast=c("Condition","dolphin-skin.Post","dolphin-skin.Pre"))
result_A_B[3]=results(dds1,contrast=c("Condition","dolphin-rectum.Post","dolphin-rectum.Pre"))
result_A_B[4]=results(dds1,contrast=c("Condition","human-hand.Post","human-hand.Pre"))
result_A_B[5]=results(dds1,contrast=c("Condition","human-nose.Post","human-nose.Pre"))
result_A_B[6]=results(dds1,contrast=c("Condition","food.Post","food.Pre"))
result_A_B[7]=results(dds1,contrast=c("Condition","water.Post","water.Pre"))
result_A_B[8]=results(dds1,contrast=c("Condition","air.Post","air.Pre"))

result_A_B_filt <- NULL
result_A_B_filt=lapply(result_A_B, function(x) {
  x = x[which(x$padj < 0.001 & abs(x$log2FoldChange)>=2),]
  x <- x[complete.cases(x),] ## to remove rows with NA
  print(x@nrows)
  return(as.list(rownames(x)))
})
otu_names_selected=unique(unlist(result_A_B_filt))

otu_deseq2=merge(otu_norm,otu[,c("taxonomy","OTUID")],by='row.names')
otu_deseq2=otu_deseq2[which(otu_deseq2$OTUID %in% otu_names_selected),-1]#Select and Remove Row.names extra column
otu_deseq2=otu_deseq2[,c("OTUID",rownames(metadata),"taxonomy"),]
write.table(otu_deseq2,paste0(filenamein,'.DESeq2.rerun.txt'),quote = FALSE,row.names = FALSE,sep="\t")
