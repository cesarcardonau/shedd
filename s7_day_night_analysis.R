require(plyr)
require(vegan)
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
`%ni%`=Negate(`%in%`)

############# load your data ##########################

uwunifrac<- read.table(paste("inputfiles/unweighted_unifrac_shedd.IL.core.txt.gz",sep=""), header=TRUE, row.names = 1,sep="\t")
wunifrac<- read.table(paste("inputfiles/weighted_unifrac_shedd.IL.core.txt.gz",sep=""), header=TRUE, row.names = 1,sep="\t")

dolphin=map_core[map_core$Source=="dolphin",] 
t_rectum=dolphin[dolphin$Source2=="dolphin-rectum",]
t_chuff=dolphin[dolphin$Source2=="dolphin-chuff",]
t_skin=dolphin[dolphin$Source2=="dolphin-skin",]

multi=map_core[ map_core$Date %in% c("10/14/14","09/29/14","11/04/14")&map_core$Source=="dolphin",] 
multi$Before10=ifelse(multi$TimeSequence == 1,1,0)

t_rectum_multi=multi[multi$Source2=="dolphin-rectum",]
t_chuff_multi=multi[multi$Source2=="dolphin-chuff",]
t_skin_multi=multi[multi$Source2=="dolphin-skin",]

water=map_core[map_core$Source=="water",] 
multi_water=map_core[ map_core$Date %in% c("10/14/14","09/29/14","11/04/14")&map_core$Source=="water",] 
multi_water$Before10=ifelse(multi_water$TimeSequence == 1,1,0)

############# counts between groups ##########################
ddply(t_chuff_multi, .(TimeWords,Source2),numcolwise(mean))
summarySE(data=t_chuff_multi,"PD_whole_tree", groupvars="TimeWords", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
ttest_compare(t_chuff_multi,"Before10","PD_whole_tree","Alpha Diversity by time (dolphin-rectum only)")


ddply(t_skin_multi, .(TimeWords,Source2),numcolwise(mean))
summarySE(data=t_skin_multi,"PD_whole_tree", groupvars="TimeWords", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
ttest_compare(t_skin_multi,"Before10","PD_whole_tree","Alpha Diversity by time (dolphin-rectum only)")


ddply(t_rectum_multi, .(Before10,TimeWords,Source2),numcolwise(length))
ddply(t_rectum_multi, .(Before10,Source2),numcolwise(length))
summarySE(data=t_rectum_multi,"PD_whole_tree", groupvars="Source2", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
ttest_compare(t_rectum_multi,"Before10","PD_whole_tree","Alpha Diversity by time (dolphin-rectum only)")

ddply(multi_water, .(Before10,TimeWords,Source2),numcolwise(mean))
summarySE(data=multi_water,"PD_whole_tree", groupvars="TimeWords", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
summarySE(data=multi_water,"PD_whole_tree", groupvars="Source2", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
ttest_compare(multi_water,"Before10","PD_whole_tree","Alpha Diversity by time (dolphin-rectum only)")



##CCNote running deseq2_fx for hourly changes dataset
metadata=rbind(multi_water,t_rectum_multi)
#input data, otu_in file should have a OTUID
otu_in=t_Norare[,c("OTUID",rownames(metadata))]
rownames(otu_in)=otu_in$OTUID
otu_in$OTUID=NULL
otu_deseq2=deseq2_fx(otu_in,metadata,field_compare = "Before10", field_subgroups = "Source2")

rownames(otu_deseq2)=otu_deseq2$OTUID
taxa=t_OTU2[rownames(t_OTU2) %in% otu_deseq2$OTUID,(ncol(t_OTU2)-9):ncol(t_OTU2)]
rownames(taxa)=taxa$OTUID
otu_deseq2_taxa=merge_by_row_name(otu_deseq2[ , colnames(otu_deseq2) %ni% rownames(metadata)],taxa)
write.csv(otu_deseq2_taxa,'Deseq2.taxa.hourly.result.csv',quote = F,row.names = T)



############# beta diversity differences between groups ##########################
#############unweighted unifrac##########################
adonis.anosim.test(t_rectum_multi,uwunifrac,"TimeWords")
adonis.anosim.test(t_chuff_multi,uwunifrac,"TimeWords")
adonis.anosim.test(t_skin_multi,uwunifrac,"TimeWords")
#ddply(dolphin, .(TimeWords,Source2),numcolwise(length))
adonis.anosim.test(t_rectum,uwunifrac,"TimeWords")
adonis.anosim.test(t_chuff,uwunifrac,"TimeWords")
adonis.anosim.test(t_skin,uwunifrac,"TimeWords")
#############


adonis.anosim.test(t_rectum_multi,wunifrac,"TimeWords")
adonis.anosim.test(t_chuff_multi,wunifrac,"TimeWords")
adonis.anosim.test(t_skin_multi,wunifrac,"TimeWords")
###############
adonis.anosim.test(t_rectum,wunifrac,"TimeWords")
adonis.anosim.test(t_chuff,wunifrac,"TimeWords")
adonis.anosim.test(t_skin,wunifrac,"TimeWords")

#############diversity changes##########################
ddply(t_rectum_multi, .(TimeWords,Source2),numcolwise(mean))
#############plot mean and sem ##########################
plot_mean_sem_time <-function(dt,field="PD_whole_tree") {
  myData=summarySE(data=dt,field, groupvars="TimeWords", na.rm=FALSE,conf.interval=.95, .drop=TRUE)
  myData$mean=myData[,field]
  myData$names=myData$TimeWords
  
  dodge <- position_dodge(width = 0.9)
  limits <- aes(ymax = myData$mean + myData$se,
                ymin = myData$mean - myData$se)
  
  p <- ggplot(data = myData, aes(x = names, y = mean, fill = names, group=1))
  
  #p + geom_bar(stat = "identity", position = dodge) +
  p + geom_line(position = dodge,color="red") +
    labs(x="Samping Times",y=field)+
    geom_errorbar(limits, position = dodge, width = 0.25) +
    theme(axis.text.x = element_text(angle = 90, hjust = 1))+ 
    theme(strip.text.x = element_text(size = 15, colour = "black", angle = 90))+
    theme(axis.line = element_line(colour = "black"),
    panel.grid.minor = element_blank(), 
           panel.grid.major = element_blank(),
           panel.background = element_blank(),
           plot.background = element_blank())
  #ggsave('Ta.pdf',width = 15,height = 8,dpi = 300)
}
plot_mean_sem_time(t_rectum_multi)
plot_mean_sem_time(t_rectum_multi,"shannon")
plot_mean_sem_time(t_rectum_multi,"observed_species")


#############diversity changes##########################


require(metagenomeSeq)
