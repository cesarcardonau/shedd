rm(list=ls())
otu<- read.table("inputfiles/shedd.IL.dolph.dt0.col.top.txt", header=TRUE, sep="\t",stringsAsFactors = FALSE)
norm_map<- read.table("inputfiles/shedd.IL.dolph.dt0.num_samples_per_col", header=FALSE, sep="\t",stringsAsFactors = FALSE)

asNumeric <- function(x) as.numeric(as.character(x))
factorsNumeric <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                   asNumeric))
factorsCharacter <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                   as.character))
#norm_map=factorsCharacter(norm_map)
otu=factorsCharacter(otu)
colnames(norm_map)=c('Group','PriorSites','Norm')
norm_map$Type=as.factor(gsub("\\..*$", "", norm_map$Group))
norm_map$Type=gsub("-",".",norm_map$Type)
norm_map$Day=as.factor(gsub("^.*\\.", "", norm_map$Group))
norm_map$PrePost=as.factor(ifelse(as.numeric(as.character(norm_map$Day))<=18,'Pre','Post'))
norm_map$Condition=as.factor(paste0(norm_map$Type,".",norm_map$PrePost))
norm_map$Group=gsub("-",".",norm_map$Group)
norm_map$Test1=ifelse(norm_map$Condition=="dolphin.nose.Post"|norm_map$Condition=="human.hand.Post","ComboPost",
                  ifelse(norm_map$Condition=="dolphin.nose.Pre"|norm_map$Condition=="human.hand.Pre","ComboPre","NotCombo"))

##Unsured if I need to pass this version of normalized counts to bnanjo
lapply(norm_map$Group,function(group_i) {otu[,gsub("-",".",group_i)]})
lapply(norm_map$Group,function(group_i) {as.integer(norm_map[norm_map$Group==group_i,'Norm'])})
##otu1 columns should line up with otu norm column for the lapply to work
##lapply allows columns to be in different order than mapping norm_map column
otu1=otu[,-c(1,ncol(otu))]
rownames(otu1)=otu$OTUID
otu_norm=otu1
otu_norm[]=as.data.frame(lapply(colnames(otu1),function(group_i){
  return(round(otu1[,group_i]/as.integer(norm_map[norm_map$Group==group_i,'Norm'])))
}))
otu_norm1=cbind(rownames(otu_norm),(otu_norm+1))
###########
#make factors for treatment column.


library(DESeq2)
#construct DESeq object from input matrix (countdata)
dds_def = DESeqDataSetFromMatrix(countData = otu_norm1, colData = norm_map, tidy = TRUE,
                          #   design = ~ group + treatment + group:treatment)
                          design = ~ Condition)
#tidy: for matrix input: whether the first column of countData is the rownames for the
##converting counts to integer mode
#count matrix
dds1 = DESeq(dds_def)
###
#Estimating size factors
#Error in estimateSizeFactorsForMatrix(counts(object), locfunc = locfunc,  : 
# every gene contains at least one zero, cannot compute log geometric means, adding +1 to counts to solve
#how to understand results https://rdrr.io/bioc/DESeq2/man/results.html
dds1@design
dds1@colData
res1 = results(dds1)
summary(res1)
res1 = res1[order(res1$padj),]
degs = res1[which(res1$padj < 0.001),]
degs #Degs
resultsNames(dds1)
result_A_B_0=results(dds1,contrast=c("Test1","ComboPost","ComboPre"))
result_A_B_1=results(dds1,contrast=c("Condition","dolphin.nose.Post","dolphin.nose.Pre"))
result_A_B_2=results(dds1,contrast=c("Condition","human.hand.Post","human.hand.Pre"))
result_A_B_3=results(dds1,contrast=list(c("Conditiondolphin.nose.Post","Conditionhuman.hand.Post"),
                                       c("Conditiondolphin.nose.Pre","Conditionhuman.hand.Pre")))
result_A_B_3a=results(dds1,contrast=list(c("Conditiondolphin.nose.Post","Conditionhuman.hand.Post"),
                                        c("Conditiondolphin.nose.Pre","Conditionhuman.hand.Pre")), listValues=c(1/2, -1/2) )


result_A_B_0 = result_A_B_0[which(result_A_B_0$padj < 0.001),]
result_A_B_0 <- result_A_B_0[complete.cases(result_A_B_0),] ## to remove rows with NA

result_A_B_1 = result_A_B_1[which(result_A_B_1$padj < 0.001),]
result_A_B_1 <- result_A_B_1[complete.cases(result_A_B_1),] ## to remove rows with NA
result_A_B_2 = result_A_B_2[which(result_A_B_2$padj < 0.001),]
result_A_B_2 <- result_A_B_2[complete.cases(result_A_B_2),] ## to remove rows with NA
result_A_B_3 = result_A_B_3[which(result_A_B_3$padj < 0.001),]
result_A_B_3 <- result_A_B_3[complete.cases(result_A_B_3),] ## to remove rows with NA
result_A_B_3a = result_A_B_3[which(result_A_B_3$padj < 0.001),]
result_A_B_3a <- result_A_B_3[complete.cases(result_A_B_3),] ## to remove rows with NA



result_A_B_0
result_A_B_1
result_A_B_2
`%ni%` <- Negate(`%in%`)

table(rownames(result_A_B_3) %in% rownames(result_A_B_0))
table(rownames(result_A_B_0) %in% rownames(result_A_B_3))


result_A_B=results(dds1,contrast=list(c("Conditiondolphin.nosePost",
                                        "Conditionhuman.handPost",
                                        "Conditionfood.aquariumPost",
                                        "Conditiondolphin.stoolPost",
                                        "Conditionair.settling.platePost"),
                                      c( "Conditiondolphin.nosePre", 
                                         "Conditionhuman.handPre",
                                         "Conditionfood.aquariumPre",
                                         "Conditiondolphin.stoolPre",
                                         "Conditionair.settling.platePre")))
result_A_B = result_A_B[which(result_A_B$padj < 0.001),]
result_A_B <- result_A_B[complete.cases(result_A_B),] ## to remove rows with NA
result_A_B


