
countMatrix = as.data.frame(t(read.delim("inputfiles/Shedd.Chrono.txt.interp.txt",sep=' ',row.names = 1,header=TRUE)))
coldata = data.frame(row.names = c('genotype1-1', 'genotype1-2', 'genotype2-1', 
                    'genotype2-2', 'genotype3-1', 'genotype3-2', 'genotype4-1', 'genotype4-2',
                    'genotype1-1_treated', 'genotype1-2_treated', 'genotype2-1_treated', 
                    'genotype2-2_treated', 'genotype3-1_treated', 'genotype3-2_treated', 
                    'genotype4-1_treated', 'genotype4-2_treated'),group = rep(c("gt1","gt2","gt3","gt4"),
                    2,each = 2),treatment = rep(c("control","treated"),each = 8))
asNumeric <- function(x) as.numeric(as.character(x))
factorsNumeric <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                   asNumeric))
factorsCharacter <- function(d) modifyList(d, lapply(d[, sapply(d, is.factor)],   
                                                     as.character))
countMatrix=factorsNumeric(countMatrix)

#make factors for treatment column.
coldata$treatment = factor(x = coldata$treatment,levels = c('treated','control'))

####check
table(colnames(countMatrix)==rownames(coldata))

library(DESeq2)
#construct DESeq object from input matrix (countdata)
dds = DESeqDataSetFromMatrix(countData = round(countMatrix[1:40,1:16]+1), colData = coldata, 
                             design = ~ group + treatment + group:treatment)

dds = DESeq(dds)
res = results(dds)
res = res[order(res$padj),]
res

#get degs based on fdr < 0.1
degs = res[which(res$padj < 0.1),]
degs #Degs

result_A_B <- results(dds, contrast=c("group","gt1","gt4")) ## contrast specifies conditions to be tested
result_A_B <- result_A_B[complete.cases(result_A_B),] ## to remove rows with NA
result_A_B
##--------------------------------------------------------------------------------##
##https://rdrr.io/bioc/DESeq2/man/results.html
## Example 1: two-group comparison

dds <- makeExampleDESeqDataSet(m=4)

dds <- DESeq(dds)
res <- results(dds, contrast=c("condition","B","A"))
res
res[which(res$padj < 0.001),]
# with more than two groups, the call would look similar, e.g.:
# results(dds, contrast=c("condition","C","A"))
# etc.

## Example 2: two conditions, two genotypes, with an interaction term

dds <- makeExampleDESeqDataSet(n=100,m=12)
dds$genotype <- factor(rep(rep(c("I","II"),each=3),2))

design(dds) <- ~ genotype + condition + genotype:condition
dds <- DESeq(dds) 
resultsNames(dds)
counts(dds)
sizeFactors(dds)
counts(dds, normalized=TRUE)
dds@colData

# Note: design with interactions terms by default have betaPrior=FALSE
resultsNames(dds)
#[1] "Intercept"             "genotype_II_vs_I"      "condition_B_vs_A"      "genotypeII.conditionB"
 
# the condition effect for genotype I (the main effect)
results(dds, contrast=c("condition","B","A"))

# the condition effect for genotype II
# this is, by definition, the main effect *plus* the interaction term
# (the extra condition effect in genotype II compared to genotype I).
results(dds, list( c("condition_B_vs_A","genotypeII.conditionB") ))

# the interaction term, answering: is the condition effect *different* across genotypes?
results(dds, name="genotypeII.conditionB")

## Example 3: two conditions, three genotypes

# ~~~ Using interaction terms ~~~

dds <- makeExampleDESeqDataSet(n=100,m=18)
dds$genotype <- factor(rep(rep(c("I","II","III"),each=3),2))
design(dds) <- ~ genotype + condition + genotype:condition
dds <- DESeq(dds)
resultsNames(dds)

# the condition effect for genotype I (the main effect)
results(dds, contrast=c("condition","B","A"))

# the condition effect for genotype III.
# this is the main effect *plus* the interaction term
# (the extra condition effect in genotype III compared to genotype I).
results(dds, contrast=list( c("condition_B_vs_A","genotypeIII.conditionB") ))

# the interaction term for condition effect in genotype III vs genotype I.
# this tests if the condition effect is different in III compared to I
results(dds, name="genotypeIII.conditionB")

# the interaction term for condition effect in genotype III vs genotype II.
# this tests if the condition effect is different in III compared to II
results(dds, contrast=list("genotypeIII.conditionB", "genotypeII.conditionB"))

# Note that a likelihood ratio could be used to test if there are any
# differences in the condition effect between the three genotypes.

# ~~~ Using a grouping variable ~~~

# This is a useful construction when users just want to compare
# specific groups which are combinations of variables.

dds$group <- factor(paste0(dds$genotype, dds$condition))
design(dds) <- ~ group
dds <- DESeq(dds)
resultsNames(dds)

# the condition effect for genotypeIII
results(dds, contrast=c("group", "IIIB", "IIIA"))
